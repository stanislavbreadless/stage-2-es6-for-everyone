import { controls } from '../../constants/controls';

function reduceHealth(gameState, fighter, amount) {
  const {
    firstFighter,
  } = gameState;

  fighter.health -= amount;
  if (fighter.health < 0)
    fighter.health = 0;

  if (fighter === firstFighter) {
    console.log('reduced first fighter health');
    console.log(fighter.health);
    const leftHealthBar = document.getElementById('left-fighter-indicator');
    leftHealthBar.style.width = (fighter.health / fighter.maxHealth) * 100 + '%';
  }
  else {
    const rightHealthBar = document.getElementById('right-fighter-indicator');
    rightHealthBar.style.width = (fighter.health / fighter.maxHealth) * 100 + '%';
  }
}

function checkHealth(firstFighter, secondFighter, endGame) {
  if (firstFighter.health <= 0)
    endGame(secondFighter);
  if (secondFighter.health <= 0)
    endGame(firstFighter);
}

function checkForCombo(pressedKeys, comboKeys) {
  return comboKeys.every(x => pressedKeys.has(x));
}

function checkAttackAndBlockEvents(gameState, code) {
  const {
    firstFighter,
    secondFighter,
  } = gameState;

  switch (code) {
    case controls.PlayerOneAttack:
      if (!firstFighter.isBlocking && !secondFighter.isBlocking)
        reduceHealth(gameState, secondFighter, getDamage(firstFighter, secondFighter));
      break;
    case controls.PlayerOneBlock:
      firstFighter.isBlocking = true;
      break;
    case controls.PlayerTwoAttack:
      if (!firstFighter.isBlocking && !secondFighter.isBlocking)
        reduceHealth(gameState, firstFighter, getDamage(secondFighter, firstFighter));
      break;
    case controls.PlayerTwoBlock:
      secondFighter.isBlocking = true;
      break;
  }
}

function checkComboEvents(gameState) {
  const {
    firstFighter,
    secondFighter,
    pressedKeys
  } = gameState;

  if (firstFighter.isComboAvaliable && !firstFighter.isBlocking
    && checkForCombo(pressedKeys, controls.PlayerOneCriticalHitCombination)) {

    reduceHealth(gameState, secondFighter, firstFighter.attack * 2)

    firstFighter.isComboAvaliable = false;
    setTimeout(() => firstFighter.isComboAvaliable = true, 10000);
  }

  if (secondFighter.isComboAvaliable && !secondFighter.isBlocking
    && checkForCombo(pressedKeys, controls.PlayerTwoCriticalHitCombination)) {

    reduceHealth(gameState, firstFighter, secondFighter.attack * 2);

    secondFighter.isComboAvaliable = false;
    setTimeout(() => secondFighter.isComboAvaliable = true, 10000);
  }
}

function getKeyboardControls(firstFighter, secondFighter, endGame) {

  const gameState = {
    pressedKeys: new Set(),
    firstFighter,
    secondFighter
  }

  return {
    keyDownController(e) {
      gameState.pressedKeys.add(e.code);

      checkAttackAndBlockEvents(gameState, e.code);
      checkComboEvents(gameState);

      checkHealth(firstFighter, secondFighter, endGame);
    },

    keyUpController(e) {
      switch (e.code) {
        case controls.PlayerOneBlock:
          firstFighter.isBlocking = false;
          break;
        case controls.PlayerTwoBlock:
          secondFighter.isBlocking = false;
          break;
      }
      gameState.pressedKeys.delete(e.code);
    }
  }
}

function getArenaFighter(fighter) {
  return {
    health: fighter.health,
    maxHealth: fighter.health,
    attack: fighter.attack,
    defense: fighter.defense,
    isBlocking: false,
    isComboAvaliable: true,
    getRealFighterObj() {
      return fighter;
    }
  }
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    firstFighter = getArenaFighter(firstFighter);
    secondFighter = getArenaFighter(secondFighter);

    const endGame = (winner) => {
      console.log('Game has ended and the winner is ', winner.getRealFighterObj());
      window.removeEventListener('keyup', keyUpController);
      window.removeEventListener('keydown', keyDownController);
      resolve(winner.getRealFighterObj());
    }
    const { keyUpController, keyDownController } = getKeyboardControls(firstFighter, secondFighter, endGame);

    window.addEventListener('keyup', keyUpController);
    window.addEventListener('keydown', keyDownController);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage >= 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = 1 + Math.random();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + Math.random();
  return fighter.defense * dodgeChance;
}
