import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter)
    return;

  const fighterImage = createFighterImage(fighter);
  const fighterName = createFighterTextDiv(fighter, 'Name');
  const fighterHelth = createFighterTextDiv(fighter, 'Health');
  const fighterAttack = createFighterTextDiv(fighter, 'Attack');
  const fighterDefense = createFighterTextDiv(fighter, 'Defense');

  fighterElement.appendChild(fighterImage);
  fighterElement.appendChild(fighterName);
  fighterElement.appendChild(fighterHelth);
  fighterElement.appendChild(fighterAttack);
  fighterElement.appendChild(fighterDefense);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterTextDiv(fighter, attr) {
  const textElement = createElement({
    tagName: 'div',
    className: 'fighter-preview___text',
  });
  textElement.innerText = `${attr}: ${fighter[attr.toLowerCase()]}`;
  return textElement;
}
