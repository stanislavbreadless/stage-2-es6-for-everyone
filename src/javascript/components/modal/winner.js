import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const winnerDescription = createElement({
    tagName: 'div',
    className: 'modal-body',
  });

  const winnerPicture = createElement({
    tagName: 'img',
    className: 'modal-img',
    attributes: {
      src: fighter.source
    }
  })

  winnerDescription.append(winnerPicture);

  showModal({
    title: `${fighter.name} Wins!`,
    bodyElement: winnerDescription
  })
}
